﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Deliverys.Context;
using Deliverys.Models.AddExit;

namespace Deliverys.Controllers
{
    public class ExitProductController : Controller
    {
        private PostsqldbContext db = new PostsqldbContext();

        // GET: ExitProduct
        public ActionResult Index()
        {
            return View(db.ExitProductModels.ToList());
        }

        // GET: ExitProduct/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExitProductModel exitProductModel = db.ExitProductModels.Find(id);
            if (exitProductModel == null)
            {
                return HttpNotFound();
            }
            return View(exitProductModel);
        }

        // GET: ExitProduct/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ExitProduct/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idexit,salidaentrega,iddelivery,idorder")] ExitProductModel exitProductModel)
        {
            if (ModelState.IsValid)
            {
                var idproduct = db.OdersModels.Where(w => w.idorder == exitProductModel.idorder)
                    .Select(s => new { s.idproduct, s.cantidad }).FirstOrDefault();
                var updateInventory = db.inventario.Find(idproduct.idproduct);
                updateInventory.stock = updateInventory.stock - idproduct.cantidad;
                db.Entry(updateInventory).State = EntityState.Modified;
                db.SaveChanges();

                db.ExitProductModels.Add(exitProductModel);
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            return View(exitProductModel);
        }

        // GET: ExitProduct/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExitProductModel exitProductModel = db.ExitProductModels.Find(id);
            if (exitProductModel == null)
            {
                return HttpNotFound();
            }
            return View(exitProductModel);
        }

        // POST: ExitProduct/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idexit,salidaentrega,iddelivery,idorder")] ExitProductModel exitProductModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(exitProductModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(exitProductModel);
        }

        // GET: ExitProduct/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ExitProductModel exitProductModel = db.ExitProductModels.Find(id);
            if (exitProductModel == null)
            {
                return HttpNotFound();
            }
            return View(exitProductModel);
        }

        // POST: ExitProduct/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ExitProductModel exitProductModel = db.ExitProductModels.Find(id);
            db.ExitProductModels.Remove(exitProductModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
