﻿using Deliverys.Models.AddExit;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;


namespace Deliverys.Models.Delivery
{
    public class DeliveryModel
    {
        public DeliveryModel()
        {
            this.deliveryproduct = new HashSet<ExitProductModel>();
        }

        [Key]
        [Display(Name = "Código Repartidor")]
        public int iddelivery { get; set; }

        [Display(Name = "Nombres")]
        [Column(TypeName = "Varchar")]
        [StringLength(50)]
        [MinLength(4)]
        public string nombreDelivery { get; set; }

        [Display(Name = "Apellidos")]
        [Column(TypeName = "Varchar")]
        [StringLength(50)]
        [MinLength(10)]
        public string apellidoDelivery { get; set; }

        [Display(Name = "DPI")]
        [Column(TypeName = "Varchar")]
        [StringLength(13)]
        [MinLength(13)]
        public string DPI { get; set; }

        [Display(Name = "Telefono")]
        [Column(TypeName = "Varchar")]
        [StringLength(8)]
        [MinLength(8)]
        public string telefono { get; set; }

        [Display(Name = "Tipo de Vehiculo")]
        [Column(TypeName = "Varchar")]
        [StringLength(25)]
        [MinLength(4)]
        public string tipoVehiculo { get; set; }

        [Display(Name = "Correo")]
        [Column(TypeName = "Varchar")]
        [StringLength(50)]
        [MinLength(5)]
        public string correoDelivery { get; set; }

        public virtual ICollection<ExitProductModel> deliveryproduct { get; set; }
    }
}