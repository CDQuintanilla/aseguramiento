﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Deliverys.Context;
using Deliverys.Models.Orders;

namespace Deliverys.Controllers
{
    public class OdersController : Controller
    {
        private PostsqldbContext db = new PostsqldbContext();
        public static int id;

        // GET: Oders
        public ActionResult Index()
        {
            id = int.Parse(GetInformation.GlobalID);
            return View(db.OdersModels.ToList());
        }

        // GET: Oders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OdersModel odersModel = db.OdersModels.Find(id);
            if (odersModel == null)
            {
                return HttpNotFound();
            }
            return View(odersModel);
        }

        // GET: Oders/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Oders/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idorder,iduser,idproduct,cantidad")] OdersModel odersModel)
        {
            if (ModelState.IsValid)
            {
                var create = new OdersModel();
                create.iduser = int.Parse(GetInformation.GlobalID);
                create.idproduct = odersModel.idproduct;
                create.cantidad = odersModel.cantidad;
                db.OdersModels.Add(create);
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            return View(odersModel);
        }

        // GET: Oders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OdersModel odersModel = db.OdersModels.Find(id);
            if (odersModel == null)
            {
                return HttpNotFound();
            }
            return View(odersModel);
        }

        // POST: Oders/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idorder,iduser,idproduct,cantidad")] OdersModel odersModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(odersModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(odersModel);
        }

        // GET: Oders/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            OdersModel odersModel = db.OdersModels.Find(id);
            if (odersModel == null)
            {
                return HttpNotFound();
            }
            return View(odersModel);
        }

        // POST: Oders/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            OdersModel odersModel = db.OdersModels.Find(id);
            db.OdersModels.Remove(odersModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
