﻿using Deliverys.Models.AddExit;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Deliverys.Models.Orders
{
    [Table("Orders")]
    public class OdersModel
    {
        public OdersModel()
        {
            this.ExitProducts = new HashSet<ExitProductModel>();
        }

        [Key]
        [Display(Name = "Código Pedido")]
        public int idorder { get; set; }

        [Display(Name = "Código Cliente")]
        public virtual Nullable<int> iduser { get; set; }

        [Required]
        [Display(Name = "Código Producto")]
        public virtual int idproduct { get; set; }

        [Required]
        [Display(Name = "Cantidad de pedido")]
        public int cantidad { get; set; }

        public virtual ICollection<ExitProductModel> ExitProducts { get; set; }
    }
}