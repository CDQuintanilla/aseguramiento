﻿using Deliverys.Models.Orders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Deliverys.Models.Home
{
    [Table("UserRegister")]
    public class RegisterModel
    {
        public RegisterModel()
        {
            this.OdersClient = new HashSet<OdersModel>();
        }

        [Key]
        [Display(Name = "ID usuario")]
        public int iduser { get; set; }

        [Display(Name = "Nombre")]
        [Column(TypeName ="Varchar")]
        [StringLength(100)]
        [MinLength(3)]
        public string nombre { get; set; }

        [Display(Name = "Apellido")]
        [Column(TypeName = "Varchar")]
        [StringLength(100)]
        [MinLength(3)]
        public string apellido { get; set; }

        [Display(Name = "Teléfono")]
        [Column(TypeName = "Varchar")]
        [StringLength(100)]
        public string telefono { get; set; }

        [Display(Name = "Correo Electrónico")]
        [Column(TypeName = "Varchar")]
        [StringLength(100)]
        public string correo { get; set; }

        [Display(Name = "Contraseña")]
        [Column(TypeName = "Varchar")]
        [StringLength(100)]
        [MinLength(10)]
        public string contrasea { get; set; }

        [Display(Name = "Dirección")]
        [Column(TypeName = "Varchar")]
        [StringLength(100)]
        public string direccion { get; set; }

        public Nullable<bool> tipopersona { get; set; }

        public virtual ICollection<OdersModel> OdersClient { get; set; }
    }
}