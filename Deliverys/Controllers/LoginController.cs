﻿using Deliverys.Context;
using Deliverys.Models.Home;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Deliverys.Controllers
{
    public class LoginController : Controller
    {
        private PostsqldbContext db = new PostsqldbContext();        

        public ActionResult IndexClient()
        {
            return View();
        }

        public ActionResult IndexUser()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LoginUser([Bind(Include = "iduser,nombre,apellido,telefono,correo,contrasea,direccion")] RegisterModel registerModel)
        {
            if (registerModel.correo == null && registerModel.contrasea == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var Login = db.registrar
                .Where(w => w.correo == registerModel.correo && w.contrasea == registerModel.contrasea)
                .Select(s => new { s.tipopersona , s.iduser})
                .FirstOrDefault();

            GetInformation.getId(Login.iduser);
            if(Login.tipopersona.Equals(null))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            else if(Login.tipopersona.Equals(true))
                return RedirectToAction("IndexClient");
            else
                return RedirectToAction("IndexUser");
        }

        public ActionResult LoginUser()
        {
            return View();
        }
        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}