PGDMP         0            	    x            aseguramiento    12.3    12.3 3    A           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            B           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            C           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            D           1262    17291    aseguramiento    DATABASE     �   CREATE DATABASE aseguramiento WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Guatemala.1252' LC_CTYPE = 'Spanish_Guatemala.1252';
    DROP DATABASE aseguramiento;
                admindb    false                        2615    17292    dbo    SCHEMA        CREATE SCHEMA dbo;
    DROP SCHEMA dbo;
                admindb    false            �            1259    17342    DeliveryModels    TABLE     ;  CREATE TABLE dbo."DeliveryModels" (
    iddelivery integer NOT NULL,
    "nombreDelivery" character varying(50),
    "apellidoDelivery" character varying(50),
    "DPI" character varying(13),
    telefono character varying(8),
    "tipoVehiculo" character varying(25),
    "correoDelivery" character varying(50)
);
 !   DROP TABLE dbo."DeliveryModels";
       dbo         heap    admindb    false    5            �            1259    17340    DeliveryModels_iddelivery_seq    SEQUENCE     �   CREATE SEQUENCE dbo."DeliveryModels_iddelivery_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE dbo."DeliveryModels_iddelivery_seq";
       dbo          admindb    false    5    210            E           0    0    DeliveryModels_iddelivery_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE dbo."DeliveryModels_iddelivery_seq" OWNED BY dbo."DeliveryModels".iddelivery;
          dbo          admindb    false    209            �            1259    17364    Exits    TABLE     �   CREATE TABLE dbo."Exits" (
    idexit integer NOT NULL,
    salidaentrega date NOT NULL,
    iddelivery integer NOT NULL,
    idorder integer NOT NULL
);
    DROP TABLE dbo."Exits";
       dbo         heap    admindb    false    5            �            1259    17362    Exits_idexit_seq    SEQUENCE     �   CREATE SEQUENCE dbo."Exits_idexit_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE dbo."Exits_idexit_seq";
       dbo          admindb    false    5    212            F           0    0    Exits_idexit_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE dbo."Exits_idexit_seq" OWNED BY dbo."Exits".idexit;
          dbo          admindb    false    211            �            1259    17314 	   Inventory    TABLE     �   CREATE TABLE dbo."Inventory" (
    idproduct integer NOT NULL,
    product character varying(50),
    description character varying(150),
    stock integer NOT NULL
);
    DROP TABLE dbo."Inventory";
       dbo         heap    admindb    false    5            �            1259    17312    Inventory_idproduct_seq    SEQUENCE     �   CREATE SEQUENCE dbo."Inventory_idproduct_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE dbo."Inventory_idproduct_seq";
       dbo          admindb    false    206    5            G           0    0    Inventory_idproduct_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE dbo."Inventory_idproduct_seq" OWNED BY dbo."Inventory".idproduct;
          dbo          admindb    false    205            �            1259    17322    Orders    TABLE     �   CREATE TABLE dbo."Orders" (
    idorder integer NOT NULL,
    idproduct integer NOT NULL,
    cantidad integer NOT NULL,
    iduser integer
);
    DROP TABLE dbo."Orders";
       dbo         heap    admindb    false    5            �            1259    17320    Orders_idorder_seq    SEQUENCE     �   CREATE SEQUENCE dbo."Orders_idorder_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE dbo."Orders_idorder_seq";
       dbo          admindb    false    208    5            H           0    0    Orders_idorder_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE dbo."Orders_idorder_seq" OWNED BY dbo."Orders".idorder;
          dbo          admindb    false    207            �            1259    17295    UserRegister    TABLE     6  CREATE TABLE dbo."UserRegister" (
    iduser integer NOT NULL,
    nombre character varying(100),
    apellido character varying(100),
    telefono character varying(100),
    correo character varying(100),
    contrasea character varying(100),
    direccion character varying(100),
    tipopersona boolean
);
    DROP TABLE dbo."UserRegister";
       dbo         heap    admindb    false    5            �            1259    17293    UserRegister_iduser_seq    SEQUENCE     �   CREATE SEQUENCE dbo."UserRegister_iduser_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE dbo."UserRegister_iduser_seq";
       dbo          admindb    false    203    5            I           0    0    UserRegister_iduser_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE dbo."UserRegister_iduser_seq" OWNED BY dbo."UserRegister".iduser;
          dbo          admindb    false    202            �            1259    17304    __MigrationHistory    TABLE     �   CREATE TABLE dbo."__MigrationHistory" (
    "MigrationId" character varying(150) NOT NULL,
    "ContextKey" character varying(300) NOT NULL,
    "Model" bytea NOT NULL,
    "ProductVersion" character varying(32) NOT NULL
);
 %   DROP TABLE dbo."__MigrationHistory";
       dbo         heap    admindb    false    5            �
           2604    17345    DeliveryModels iddelivery    DEFAULT     �   ALTER TABLE ONLY dbo."DeliveryModels" ALTER COLUMN iddelivery SET DEFAULT nextval('dbo."DeliveryModels_iddelivery_seq"'::regclass);
 G   ALTER TABLE dbo."DeliveryModels" ALTER COLUMN iddelivery DROP DEFAULT;
       dbo          admindb    false    209    210    210            �
           2604    17367    Exits idexit    DEFAULT     j   ALTER TABLE ONLY dbo."Exits" ALTER COLUMN idexit SET DEFAULT nextval('dbo."Exits_idexit_seq"'::regclass);
 :   ALTER TABLE dbo."Exits" ALTER COLUMN idexit DROP DEFAULT;
       dbo          admindb    false    211    212    212            �
           2604    17317    Inventory idproduct    DEFAULT     x   ALTER TABLE ONLY dbo."Inventory" ALTER COLUMN idproduct SET DEFAULT nextval('dbo."Inventory_idproduct_seq"'::regclass);
 A   ALTER TABLE dbo."Inventory" ALTER COLUMN idproduct DROP DEFAULT;
       dbo          admindb    false    206    205    206            �
           2604    17325    Orders idorder    DEFAULT     n   ALTER TABLE ONLY dbo."Orders" ALTER COLUMN idorder SET DEFAULT nextval('dbo."Orders_idorder_seq"'::regclass);
 <   ALTER TABLE dbo."Orders" ALTER COLUMN idorder DROP DEFAULT;
       dbo          admindb    false    207    208    208            �
           2604    17298    UserRegister iduser    DEFAULT     x   ALTER TABLE ONLY dbo."UserRegister" ALTER COLUMN iduser SET DEFAULT nextval('dbo."UserRegister_iduser_seq"'::regclass);
 A   ALTER TABLE dbo."UserRegister" ALTER COLUMN iduser DROP DEFAULT;
       dbo          admindb    false    202    203    203            <          0    17342    DeliveryModels 
   TABLE DATA           �   COPY dbo."DeliveryModels" (iddelivery, "nombreDelivery", "apellidoDelivery", "DPI", telefono, "tipoVehiculo", "correoDelivery") FROM stdin;
    dbo          admindb    false    210   h;       >          0    17364    Exits 
   TABLE DATA           J   COPY dbo."Exits" (idexit, salidaentrega, iddelivery, idorder) FROM stdin;
    dbo          admindb    false    212   �;       8          0    17314 	   Inventory 
   TABLE DATA           J   COPY dbo."Inventory" (idproduct, product, description, stock) FROM stdin;
    dbo          admindb    false    206    <       :          0    17322    Orders 
   TABLE DATA           E   COPY dbo."Orders" (idorder, idproduct, cantidad, iduser) FROM stdin;
    dbo          admindb    false    208   R<       5          0    17295    UserRegister 
   TABLE DATA           t   COPY dbo."UserRegister" (iduser, nombre, apellido, telefono, correo, contrasea, direccion, tipopersona) FROM stdin;
    dbo          admindb    false    203   �<       6          0    17304    __MigrationHistory 
   TABLE DATA           c   COPY dbo."__MigrationHistory" ("MigrationId", "ContextKey", "Model", "ProductVersion") FROM stdin;
    dbo          admindb    false    204   n=       J           0    0    DeliveryModels_iddelivery_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('dbo."DeliveryModels_iddelivery_seq"', 1, true);
          dbo          admindb    false    209            K           0    0    Exits_idexit_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('dbo."Exits_idexit_seq"', 1, true);
          dbo          admindb    false    211            L           0    0    Inventory_idproduct_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('dbo."Inventory_idproduct_seq"', 1, true);
          dbo          admindb    false    205            M           0    0    Orders_idorder_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('dbo."Orders_idorder_seq"', 5, true);
          dbo          admindb    false    207            N           0    0    UserRegister_iduser_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('dbo."UserRegister_iduser_seq"', 5, true);
          dbo          admindb    false    202            �
           2606    17347 $   DeliveryModels PK_dbo.DeliveryModels 
   CONSTRAINT     k   ALTER TABLE ONLY dbo."DeliveryModels"
    ADD CONSTRAINT "PK_dbo.DeliveryModels" PRIMARY KEY (iddelivery);
 O   ALTER TABLE ONLY dbo."DeliveryModels" DROP CONSTRAINT "PK_dbo.DeliveryModels";
       dbo            admindb    false    210            �
           2606    17369    Exits PK_dbo.Exits 
   CONSTRAINT     U   ALTER TABLE ONLY dbo."Exits"
    ADD CONSTRAINT "PK_dbo.Exits" PRIMARY KEY (idexit);
 =   ALTER TABLE ONLY dbo."Exits" DROP CONSTRAINT "PK_dbo.Exits";
       dbo            admindb    false    212            �
           2606    17319    Inventory PK_dbo.Inventory 
   CONSTRAINT     `   ALTER TABLE ONLY dbo."Inventory"
    ADD CONSTRAINT "PK_dbo.Inventory" PRIMARY KEY (idproduct);
 E   ALTER TABLE ONLY dbo."Inventory" DROP CONSTRAINT "PK_dbo.Inventory";
       dbo            admindb    false    206            �
           2606    17327    Orders PK_dbo.Orders 
   CONSTRAINT     X   ALTER TABLE ONLY dbo."Orders"
    ADD CONSTRAINT "PK_dbo.Orders" PRIMARY KEY (idorder);
 ?   ALTER TABLE ONLY dbo."Orders" DROP CONSTRAINT "PK_dbo.Orders";
       dbo            admindb    false    208            �
           2606    17303     UserRegister PK_dbo.UserRegister 
   CONSTRAINT     c   ALTER TABLE ONLY dbo."UserRegister"
    ADD CONSTRAINT "PK_dbo.UserRegister" PRIMARY KEY (iduser);
 K   ALTER TABLE ONLY dbo."UserRegister" DROP CONSTRAINT "PK_dbo.UserRegister";
       dbo            admindb    false    203            �
           2606    17311 ,   __MigrationHistory PK_dbo.__MigrationHistory 
   CONSTRAINT     �   ALTER TABLE ONLY dbo."__MigrationHistory"
    ADD CONSTRAINT "PK_dbo.__MigrationHistory" PRIMARY KEY ("MigrationId", "ContextKey");
 W   ALTER TABLE ONLY dbo."__MigrationHistory" DROP CONSTRAINT "PK_dbo.__MigrationHistory";
       dbo            admindb    false    204    204            �
           1259    17370    Exits_IX_iddelivery    INDEX     L   CREATE INDEX "Exits_IX_iddelivery" ON dbo."Exits" USING btree (iddelivery);
 &   DROP INDEX dbo."Exits_IX_iddelivery";
       dbo            admindb    false    212            �
           1259    17371    Exits_IX_idorder    INDEX     F   CREATE INDEX "Exits_IX_idorder" ON dbo."Exits" USING btree (idorder);
 #   DROP INDEX dbo."Exits_IX_idorder";
       dbo            admindb    false    212            �
           1259    17355    Orders_IX_idproduct    INDEX     L   CREATE INDEX "Orders_IX_idproduct" ON dbo."Orders" USING btree (idproduct);
 &   DROP INDEX dbo."Orders_IX_idproduct";
       dbo            admindb    false    208            �
           1259    17383    Orders_IX_iduser    INDEX     F   CREATE INDEX "Orders_IX_iduser" ON dbo."Orders" USING btree (iduser);
 #   DROP INDEX dbo."Orders_IX_iduser";
       dbo            admindb    false    208            �
           2606    17372 0   Exits FK_dbo.Exits_dbo.DeliveryModels_iddelivery    FK CONSTRAINT     �   ALTER TABLE ONLY dbo."Exits"
    ADD CONSTRAINT "FK_dbo.Exits_dbo.DeliveryModels_iddelivery" FOREIGN KEY (iddelivery) REFERENCES dbo."DeliveryModels"(iddelivery) ON DELETE CASCADE;
 [   ALTER TABLE ONLY dbo."Exits" DROP CONSTRAINT "FK_dbo.Exits_dbo.DeliveryModels_iddelivery";
       dbo          admindb    false    210    212    2733            �
           2606    17377 %   Exits FK_dbo.Exits_dbo.Orders_idorder    FK CONSTRAINT     �   ALTER TABLE ONLY dbo."Exits"
    ADD CONSTRAINT "FK_dbo.Exits_dbo.Orders_idorder" FOREIGN KEY (idorder) REFERENCES dbo."Orders"(idorder) ON DELETE CASCADE;
 P   ALTER TABLE ONLY dbo."Exits" DROP CONSTRAINT "FK_dbo.Exits_dbo.Orders_idorder";
       dbo          admindb    false    212    2731    208            �
           2606    17356 ,   Orders FK_dbo.Orders_dbo.Inventory_idproduct    FK CONSTRAINT     �   ALTER TABLE ONLY dbo."Orders"
    ADD CONSTRAINT "FK_dbo.Orders_dbo.Inventory_idproduct" FOREIGN KEY (idproduct) REFERENCES dbo."Inventory"(idproduct) ON DELETE CASCADE;
 W   ALTER TABLE ONLY dbo."Orders" DROP CONSTRAINT "FK_dbo.Orders_dbo.Inventory_idproduct";
       dbo          admindb    false    208    206    2727            �
           2606    17384 ,   Orders FK_dbo.Orders_dbo.UserRegister_iduser    FK CONSTRAINT     �   ALTER TABLE ONLY dbo."Orders"
    ADD CONSTRAINT "FK_dbo.Orders_dbo.UserRegister_iduser" FOREIGN KEY (iduser) REFERENCES dbo."UserRegister"(iduser);
 W   ALTER TABLE ONLY dbo."Orders" DROP CONSTRAINT "FK_dbo.Orders_dbo.UserRegister_iduser";
       dbo          admindb    false    203    208    2723            <   Z   x�3�N-J��WpM+J���-�IT�O�I-�4426153��4��0�41��4442�K��L.���,�,-r(�LI���K������� C@      >      x�3�4202�54�52�4�4����� )G      8   B   x�3���-(J-�/JTp��C���d*���%g��%�($��)��+��$��R�9��b���� c�x      :   "   x�3�4A.#(m�M��	�)��q��qqq yD      5   �   x�U�1O�0��󯰺׊ݤ��Błԁ�r$&ru�U��������N�{�����ϙ���Jí����an�b.�I�뇽6�1�I8f-#<UFOߨ~?l;k4�#"]��VTjB�hD�o�6\o{�_SD��}�O��)�⠰k�9a����`�k����9�'1� r|.�f~�����?X���ǯVA��pΩ�}��/�VY      6      x���M�.�q�9����&��Ӟ4�3�$�]�TFտ���}e�
U�ȶ|�9{�o&�b��`0>�	O�O�%�����g����7��������������߿����?��_���_���������������p�z����������]�I��g��NO�Tzk�;�3fx��e���O-;6�<o�6�C���#�\�t�zn���1�{S\�^v
u��g��v�~G�'��ޗ����^n{��e��S3��V�*�xF��u��>�?8���w�V7��j���N�|]��6��	��Zs�㹫��3�g��YN?oKw�w��z������6c�Qg+O:�9)���RZ�<!<�$�&��'�6�ߧ�6<��}N�5���9�����Zo��xj��齏�Mvy�2���;���}�|V|r�R�g=oy���.¾��n�T���1��g�>��������-l�,3�sY��%�g��uV�W)~�<+�X�;6&�������mF�d���m��������3=���)eM��>~�=�ܗ�:卧�9y���g��dV�M���nV�<�/�5�Xj
�ܻ�l^��0j_�����?�������#��O�u���q��.�0b���8���͋�e��o���pF�z��|0���J���_�y���s�������<�q�Oc�������?�������[����F�g�������O͙7���w�s�]YȻ��5��W�W�Kk(�֛0��?�έ�k�}{�������L64���5�ԧ��_}˸m�9p�}*��V��2�b�H/Ș��7�3�Q�~�^m���<{�9��T�Ŵ#�v+�U^�jj�����G9ut -'lf� ����x�z�V�8̘��9wO8\����i����y�y��[�N��4���\��[����sC�EX�/}S�w̓�����߄�-<�4�?�7��`۱��B��ɝż,����?i�[}��3������w�����|����Xx�A�.�Y�7�i�����j|B��j�p��g��s��{����o�7-���!x>��.U�+D�����c����u�#�1�»�riN���Rg`:[�"�z@N�� �Ss���
6��?����Y��˓o/����)�b�{�wU���o���1�����pk�0�3����<�a/Y��e�w�yN�	�����b[Ag�z�{*������o�1�����m9o�Ƴp�.0��d0��b�a�F@��Zn�u^W��:��z��{�	�v�U�k{v�:�e��������}8�\���3�l#	�A��m-',��˿��d��8�fBK<��A5����h@���.�U{� �S�S%��1`S�O���+����"��9&O���[��Ŏn���a��O�!J-���]���o�K#Φ���x~��������--��C[Z�����b��������٩����Xe_����V4���"�������p���4��P��������{pd�� iꋄr�j��Y��q��$k�P�,��k�M�."�����e{���<�΅��~���d�ʰ��b'a;IT�����@�7��C]�/\�6��D0�g����5G�~W/0��`�b�/�[;��k^�=�ъ'��w����h{	��y��xc,�H��i�z������s�ܠ3�j*�x��j?�����3�N6s�E�k|f"���,k�fوŻ��4��?���Y	�Z�X��Mk5�	��v�����MY���Wxכ�t�6�J^6!ԩ��R�$/��C��w���D������H^���7x��F��@6L�4���4P֒w��F!J�3�z᭧���]�ц5��#}gI2;H�>�k����?=A$����jb͸&L�����X���H��a�M�|R����{j,݈{a��#��8S�n ���Xr�8.���7���~}�����̋b�W�[�lG%��x��o[:�R��Yc �{��
+Ʋ~��aa�u��цH�>l��\	'��l#����0��0�#4"W�8E-A�4� � k���i�'A�[�w=�եa�+�^FĚ����$�G(Q��d���/������m�"2$����&���s�oZ����� MPf�]	ij �/Ґ����K0!�F6���㙷�x� ��*�u��芼|߀d�#��QS�=T�T���x�����a�	'���y��*��wP�FBU��k�/z�]��"�%L�p�^�m'��'�V�O��;;^td��op ������|��T)�J|g�m�~�e��v����!.	���@Ap��\� �4�Ao/o6$��Ű�ۚ|"A����(e��
��ŹzNh�S;P��Od#�a�0 ��y���W;���b���_	X� f��X����|b��z z
d_:4t((�|B9N��oG$�0�W�\#�]6F�ao��r�PЃ�ī�	]��c�<�DB����4q��w"Ѧ�	�cꃄ����9���?0�����u���}6���Y��x��b��z+	�0Q2��{��%a9&��e7�!�8D����	������CL�`�Lc�[�	%m���EU�������������a����?����?�����_G}�_��̟�1"$�f�B���)�~>�F��B����H�8��@�j��%Ɉp  ՍP��B,�վ��C�fb�\�
�Q��(L��E�7��o��_6N�3�����Ɨ�$b.�M��A�Bš���/�f�S��@8��L	���B�	��Yg�^ЮJg����$
02YL �!kP��X�úG��Y��������O��w�0�wE����K|���?��Uf�YĂ$/��B�C�x H�(bh/ω���``W|���+&��gWap�3�!x_�&N�Z88�����l� _��ڷ��v'rU��8{�m@M�6�CFc��A�C��'g�Ð�<sX���� ����� G�G�-��@��r70V�A����i�Ӻ������!B3���l��\4�dIH���
	Q��&fn�FSE�s���b��%����O̯B����C^�
:��~������	r�h1�.��Ҷ0ϔ!JQ��A��<���-�fB�Yu�b�_)0v��h��w�<��n���2r�9����]"�!��J����?�X���5̀<��?�	AP��7\�m�ၒ�9=2��\P9��a��A	��b�� �s ]p��<�з�sI�N�����^�@���pO�	�;f��p"�0�D�U�$��Q�Yn4$O�*ٯ�#�q�8)�c\0�
��Ġ��Wy?���z���͊�%���y����%"_ǲ�E�aq�r��s�y�l��9f��e�+�`��9�wz@
��j�����ZL	��.W=g'��*�6��;ޛ0��k��#�K2��ƽ�Vm�� ͓K��]s7�A@�>�+B���*����^, 8����
�°�W
�:ԛ2�C����Yf������ Of��a���e����>|Rl`C��`nX>.��&�����o��a�a�v"#�*�M���[BQ�X�5�eak *�fy�ïl�l���tC`F�@Ы�a]/��T1	��Y�0�3�����K��Ѕ��d,1p�˷�ĝ�"�<F��K\�p9�&QCJS�DHc�N��Մ�=0-4����2�s6�¥1v/ ̈�F8:|,���Y���!m��lJ�`�@����I��Ǯ�"�a'x��V+;mU����/�%}�&
������d����e�n�9'�|�^�LKln�{�\�x5m��<�<��X8��s6n�����0N��c���?�3    ǎ�p�g���Wg`7�m�:�t�"b)����a6o��/6�'D0��|8F"�/��U{A�����ft{	��ynC�!1���((?�:� �$A1K�@��Za��ķ�ѯ���#Z���������p��}]�����#��H�!!.%���u���vO���� փ�����%J	��?�?�����?�����m>in���tQa��H��r0x>��6��>�>>��K���%\�XBk�׆Qì+<�W~�i�0��J��S��b�:�\�8��u@�Ƶz�՚:4 \�Ʉ&A�( L�l�0�@����L|q������`^|�%>a��a9�&�(�h���}�ҵ����\?��@�2��2���f�-̘N#�7sK)�l���[�|Ѓ\Lr$@h�X�0g��d.ڃ-��Ű���t��s��!��^�$ �J�.0&� ݇Y�4��M�f#Mb��[���4�Њ�t,��6C����#��X�R�W��f|U��M�~E�:�a�ɝ]LX�X�rA����tDe�H�=���f̵�H����Lւ[�����X0u��5��Wީ���"�:�"~|��/�����$0>�U����d��O�cQ��I���6S��x �0'0f��b3X�&� ��p�'��ccX��Ӣ�6����1<�N��ꨧbBHaY�:fI��R|�`�h�7�p�18*��w�9�4��t���.I���4�z��/Q�<-f���[=�,
�,��,�%�d8ճd6st�0~�㢠.o~Cd���Ӡ0�;�����o�epD$ߌ��Y��p
�R�('��-$��
P�ڢ���E4e���q٘v�6�5̦j�E^���u6X��F<��"<��+�]�H�/$���X+n�-�*ğݲ@	ͼ�Y�ɞy�i���0�\���
�da�TM�jC /������
@p��ZkgV���^��T�`%x�)��'�e�n�	����|���a�Y�=0��0FP��̋�8���
�Wܼ	�i����l��<�8��6���Ȕ����Z�L^u#�=%��� �M�g�Z���*X� CH�����FP����m������o��̷�Vכy�7�٪9l�:��41�=� %�ƁrZ6}�H
�a:>������o3���8#�7����BA���9�cU�����	Ŋ ��lV>T�����.���Pp:*�@9`�Һ�_>=����o"6���~,��ڵ����Z��ӈU�<�a,�%��y*T�����X�T��0s�}���yI��}��^q��Y�Xz[C]A9Nqmuh_�c����	\Ə2W�f�	I�gi!y�6qg��xLv�1�������[D �B1[�6�f�n���	j9��J A��L�=x��7�$�75(VT͉�LRk�	���]h�j*�e#8߸�c�Q�HZ�f'qN��ʗyZ[�Pz?� �ω���j�o�-؁.% $��$�E eZ;�U��l�L3���.�	ӑoc�cw���Չ����1B�H������=�x���������������7s�hG~!��������+�m�eY.ǆ�7�R�'�o#��̟��@���JԵ]�g���}T^��Уlu����x��F6�o"^�q�(�Υ�HD�;�ݹl��퇾�Kb��e�����$�6~Z����]�l9XQ+���}�Ԓ�0���e>�؋܋����Y��5��l�&N�>���,����G�*yٷ��h���1��AB�r=�K���1�/�h{&r!�1>�3�EE0$��9�y�(H
��W��赛K���ĕۍ�c�"Q;T[�u@6?�oAf�@�ȧwb(.F	P������k���n6��Ą"ۑ��:0���h�7Mu�7��gn�N�x��@L�D��g6���8��l;�)�V0��3�> �k���}��q��6�8�qN�b����
ίD�',1�Wu��#^鑍)���;(͋P3#b��ԛ��K^�\��!VP�i @�&�[��I�UUK����ҰGR����a0�ЌLD	>��f?��	����k'=!�[o:Z�L�V������hZb�E5e�j��A_F93I��;{��k�y������������[&J��0[���h ���q�r���k��R`%v�s;�#�Tb��;��(�ZI�r��������O����ٴ�h�M�pJV#.���ר\a� ��������{Y�tV�f�˧�<0����;1zB4�rv`���V!�����ր�e�<ˑ�J�D��2��){åeJ�F|�jP��p���X��+��M�NdBa��u-�ĆGVˤ�y�U�6��ne�HpB��oe�Ҳ�� j�0��֓E�������r(�Sm2��UajwB�	*�Aб��W��0f�J�a��e}*+rUtq�3��=R��Q���x3x&Lϋ�����s��î`��EU%��/ �%�Q2\�^}Y忛�ӁA#2����	r����:�e��w|�����0r4�j��<FG�O�X���h��U���T0�f�	�7�p>Y���3���I���h~�[�o�������.%3�Y�A�#,��P=�������5 )���ֱ���F���{����������	����P��FJ�x�r�R��"���ޠ�xː���7l?Ѣw�7z8���iP������BL�D�6�p���S�?��Pk�C\^̃s��*�gY<��Z�GQp��=�#p��,�>�Y/
���*�����v��eE5�6��Q{|/�!z�����s���#��d�oe	>�xY�����Ay�����';;:5#T�wRm���0����h�2����*�0=����vci`{zt⠑_�س��T���a�	:�2��/6�l�^4>2
%4�>\I���_2�y)~0;:�E�^\*��"�#�1hH�jk��Z��|`� ��1>��Ġx*1
�0_`�/��W@���D�n��b�Ůg�m���N�I=#`I(�+3�w!w�VT�Q1YP3	M䷽r{z�C���C��k�((3,D�o9��W|Y(V�T��ā<�"s�ƿ���c݋Yf4&`�_ANd� z1Pd�ˬ���i�"�q������k+�s|v`��Y�,���̪�<�j��������k!5^��%�#� �X�k�^�a��#�6^�$ j^�r�a0׊�{�`{:1�j#~��Q>�GA���A�8w�@����6�Q�*p�B��y/Lu`��W�k&~П��J@�mr�e��ya`=����`/�;~����q��{�J��}K4?�.�ր'�!�|y׈}-G��Jln�߄�(��x �e�&�������Y&X���S�ۍ�d侍��1|��i�1`,�vW�n"��h�k�5'!,�Ji�;�<S�YTՃS��?������Sz`�L˶i������l�i�	z�S9�&����;�C@'���u$rM�4z=���!J�.־`�l[5pτ����T�«����*{���'��'+��-�p}�x�z�!�߁=����u��$�Z�|��x�º�j��
�H`PcFH$Xű`��C���|�Z�#n�C�6��)���Y����-��[q��Vo��M��nF��R���M���a�O{�΄�
���F�W���v����V���3>8,RULa�j��O���=ü`#�B's�pl����y�P�\����w�hy�89�d�]��E� ^�{!�a|@x<�U#M�������^�������k�7��;����O:+_�2��T�q:
kx�l0�ɻ�0=5�R@������xċ��2�&�p[�����-|'J�3�a5=���x�d�y$��
�Y���� Y��1Y�G+ۘbu���{��其�?PÎdq*�    ��"v��cI��X��a�U�'D/r�,���2f��'5P4l�R�wV�L$�|
�ﱚ�9�=������Z��ر~=��y�	GD5h��no	�y>{[�Y&��,P#��d�=�8�����1G|�(�Q�-%T���Z���z����,T\$,��h q����
��e�t����Ě�C8?	;�8�Pş���.����I�o!+o�_����f7uwq�ď)s��j�z0���`"������s��0:8B��N.�:�����]�t�!���x0��;y�xfi��"�,L�9[v%�B �9�D��sH|������D��ۃ!�g���eF���[M0��;�n���
H�@��$��xԸ5�@i�����4��G���c���QClD��+���QO��)	 ��J�w���J�)���o�����k{�_���W_V��;�Qy�:��잓�O��M[��V���<,Hx������M������C�ͳ����6����r>�e!ҹ��e�@o!�V��I��"��鯲q9y�͐,�y�{ 
/�{���xw*�[�դ�h���(�:��5�iF),;�[��Aj�7t�
p���C��\�w��������g<���K���]�����r��H�&tO���şYR�J�[8��
�(�Ch]����V8�g�l-��=d�Y%> ����`�q�!+7�x�^��<���$?�}�����F����o9��H"i4[Nn�^(�'�6���H��ʣ��� �&A����=L�c�V�Q�� Y��`wdm&���c�h����v�g�P7��NHD�����/����/2�X�1��v6�2t����*���%#g:�h�{"ܒY���/f���~e~ʲ��E|�*���Ho��7�(�X���ǰ��xc�muJ�4���� 4�4l����MH�lַ#���tk�IG��Y"���M�bX0Dn�f�`%��}~�?��vk��)`�`��Ac�N-J��A��4��r�d�aɖ�ـ��;dLc!��D~�/N����kg��=@s�P�b���ɠ8�;��$b�~+?�E�{p Ͷ_�iGl��;Qb�������h�i�!�G��x�^�)��0�C�fA1�?�-���o2	OC��l�Ǒ,�s��'���T�y�Hܵ!����^� ~��At�����YW��wr�������� ܽ�ڃ�E�o�X�b��t�ފ,��R�7Y�F��`�%XC��8>��<��~T~�\�U��#�o�I6odǎ��Y��꿅m Rb-=�D��ݚF��g?��=O'jZ� �pl���7tpŒ���XP���_��-�*�/�_D��p$���8����5��u���n�5�$��v��qAX��?�a(l_>i�N<O�Q&h��ͩȑ� ,œ��v���{�шF�����f.�T���k�`���_�₏`!�is���Զ�ߦ�[XDF`���MfA���2 _Kf�a�,��uL�7[kq�qF<^�\)���7a��P�2��d�n�A��ӵ
�.߈\=���ڶ�4 l�T�/���?/�����@ϑ�@qX��m�������nl��' f����L�c{�`�}c�[
5�d
a|8j�ؔ�&r�P+_�z�2lh�m��}�&{^��(�eMr�ٔ��k�o�c����l ��h�A�%�|:+�&U�,� �O���Z�U����"C�.�wPt��LK�%:��cP���?%��t��=F�5g
�(�N�)�,��B;�d��ˆ�4'q�v�籝��(�������~�r"T�VG����>�߀?���	M�3G:�2��L>�cFn��Ya��P0��ԉ�0L����Κs���5�\����;x�e�m��5����b�vGG�3�0�y$�"�
�����;y'rظ�O>����k�m��w+v��&:V�dp�um"~��B�h��%���0�.E⟜�
[vh�˄jkt֒u ~����/��1�yM�Z A-"�.���z���dzl2W���T�q���,�2-44.�wR1���.!�o��Y��?��>Y�ӛ�p���!
6m�(Z!�6,�) ��00��J�'»�oz����5�@�yR}�峩���yȝݷ�gYU�M���_�-�(/u�!6����"�	AO�� �Q��5��BU�ywJ�+�!in
����ط�Y�q*���m�զ'G�S���,*6����~>���"?B�����R�YBB�)����]D�J͐�l�4[�B�2N��q�QH|�9��<x�f��!����"ގ���+�uM�5B�O{���l3��|V��3��� ��ݘL��Xn┃��p�"@h�`#)ޛm���U�jV	��E$ +8�� ����0�6m!t��(�e��i�����V�������8)��v_lt� M򟛓��X{鼱M��AHd��;B�<A2����h@�sqa���y�����t.�P�8���!���OU��I�?��n���kD�(�p���z�\}J��4�6!G�F�1���/T 4gb�o#<��<�D^�	@��kyL
�&��t�堖L�Ǎ�ϧ���z���qs/�ʺ%�m:I�){���w1�
x��0��!���6��Y��u?l;� ���$<�=�o�=��1t�ݍ�Zj&*�	��c�(C�z6b�ۈc�fڬj���CD�wB�r#ܡ�xԂ �8V,���¼���ܾ#�@0|�Y�!p&�R0e���N�3�����Ǿ�'����F@�٦f�<2����MPe[�#^@W^O><���*j�L�Z�WO�н3�8!>��O^��ݵ����X�}���c �2f��s�����W��*�cG�9*Α�6��.�	��Y�נkyf��8?��8��y�����#�G���(��oD��[��V�T��1���V�r|(l	�3���������z�<v���]?&K���n/�� ���#��Hq0L��n!�BZH�h��} }���`Hd`Y�߄�5B�񪃀j�=c��_��8~�2_�W0�z��*� F�d[[{ulO;����.'�.?�p�M���s<�#I�����=�P@���Ol���R����A���\,%n�<�ϴ���&"���)���bBd\��>m��qn�`l�II�Ӂ58O@�����cmփ���8L�������7��@�M�jEm����႟��������_O�}#}��`n�6�|�n�+�]`�ڲ!�=z슃)�_#�P~�m�Hg�4�?���6�:qsМG��Eh-�,7vc7f4)�]`wF\��xp~ߴ��{3�X�n����s@m��+���������`�d+bx��1�և׆`�kq�,W��3��Ϟxѵ���b�8ص�'�w�%H/�%B�$�+rT�yw��7��M{˟�mvOb_�,��'X�3�)���69w8Bƶ��6�2����n?#���;�w5�܋�\�Ǥ��H����p��T$�4�9��)k:Ä��io��\�e_�I#���Zp"�K��8�����d$�40�؛GL@��q�;�R(�G��,ವ<��Al�tӔ�"�/N\ޥ
���P��z`�9��J����\Q�|���kh�7�N��tA����1^�V�Yp�:l���8�`��Ӣ��2�����L�>�p���o�3�;;w�Z(�"�6E���+�l�����`��Z�7=˾��.�
�K,y:
L�3�2�/��]a��I(��wajw�ݼ�T<�_�H��o0uf�W�7��2�9��X~�Ռ�����I��q�C���l-�#~|'���j8|*��y���L��ޡ�Ĉڣ�K�gm.o�7����6|ra��<z� �G<r,߁#�X;{
@b�<m,�=�}��d�T"�ǯ����1�u-(3��l�e5˃rYf㲳�+�B�{��ƛ���J��e    �0^��塿��ݝ��O�=���
�	*�?Y��˛8=����V�9��/�]��/��kR�".�Xl��Dg xL�.�9�I������aS�(��bB����c�8���A��g�-U����%�(��<��NY��8����U%m��7<6�g�斶Qk�5��
ږ=[�S�Ψ�u�D�����w��j�lC���6A��[���ܯ��<@�
���?��N�1�|yټ�ԙ�<��ABڀ���ֶ�0ay���q�|�  �DRG�;,�ֻW`�6X8?�4���`�%&�	����v�v�'[��Z&���gc��c�C��=7D��Q�v���Bf_�mKJB��Z�s��2�������?	f[�^�J�etU���x6���ļ/M��&��,E����?�ӿ�Fh��o5BO'���E��9M�t����4Crl��?5B�c̀��7�j�6�_��C02�
��!���3�;J�/����IP_��?�3q�
y�Np � �sLd�l�Q�J��=������a���7���	tɑ���߄��$�Y��l�C4�V�	ȭsQv�w���\~�Ďt���l��b�ֱ�m)q��wD������<ؓ�{g2�f�,L�
OQ�FAJ��B��U��GO�S+����>��?�
�r��o�B���?�
�O��gt�n[��.���W+D�+��~��~CK����zV�ߵ�w1�U״��r�J��rV��G��8��E������M0�Ǉ�XNM��;`�6�'�k[~�M�V��@�U��<�ԇ���':�A��}�_�����P�OW
��ĥI�gK�&�ň���'a%�:���2�`q�E�]	��ڧ�}#m����Έ�c���=����{� ��J7@K�X�>�Kr���3������}�z�7��7&g�w����x�)(m��[p�0?��$pB(��˶��¨!^+��3��� PSu:�#�}'��C���j[*R���ub�g!��`�	}O��؛��<�/�cxN�����*���p��X�s���8{;g'Rf�*ɋ(�]��&�d�-�K��?'�G����&�v���y�?i�b�
9	<�'�B�/��R=C ����Uj�d�_+o��|#s0��38a���Os��� MLR����>W�`H�"v3��&�����L�Ʋ��zn��;dn�� ��I�p.���Q��	H��Do���X�Z|��#�
c�'�Yi���C�\LU<��iWj�r`�\\s��L'��O�.J��|�kY	�y@��[o��
y�j�8=a�0��e���ZvD�x��(yH�tB�G������${H�n��c4^*�9�o�8,҆��]��ǂ���|*�<�_T���u�������[��ˮ�m���Y�3<����د�9�		3�*�چY�Q9|�ON<��2�{*����X{���u�f�A(��s:�e�̃x�Y�� ���j�u`x��U�7�Ǐ���o�q[�5��	�8\�B8�v�Ϥ�c���b�ײ�)c˓�Ŭ;�٠�~�.�ߜ�m{DB��V�a��+�(KP�6��x�Ў6�a<�
��I9�>���S�V ^�+x�E�N�1��~� (�v<|��Q晵%<���uI[m���YK���%6�@�}���D�Z*b{<H�����u�m�ɂ�=i�eQ=|�}۴Px ��� �g��7���u�@,�ċ8����|�Qx�.��m�c�
J<�+	�������ħ[�oԊ�̰{s�v��Z+��Ǚ,ND���@OǠP�V. 6��y�`{r����wԅ���MŸ�P��!�O����ۺUyay{��L�>���FXg�>To�p�H�#����~���Θ��ȃH�5�Fa3�H����Dby��c�F6G;1;+�=x�XQ�Ć�+Ijߥ3��әy?ޡ�m�7>���=��G�>�/�̓k�É�������S���%$�*���ߪ�%�G�NE�39�g�:��%;.t�eOC<8�P�{<Ԧ���a�� k�-0�H,1�d0���!,}�/|A�ӀEe�J��?*~+#���z% ��u�������� 7gp,��_M>/I�ac�
�&�x�z�YV�6٨�-���7��5��Gz�g�7�Qb`�
g�����6	TW�:T=@�@S7MI������u�����M|�X����`|�weJv/�P׮�T;��9�9�\�	:f�:�|���y#|���/x����'8qA������"^�O�l������&9
�}�%�F�o��-AD��L�V��f�w�Om?J�r�S}��+�O�r1 +4���ѳ�2�h���o���6�����G#L����I��A�<�A$!b 5.�g�tq��v�߳a��\]vT�s���|*�95��"<�}��x��KA������ċXL�O+������c�šXYB	'�B�EdF4�<�^E�^B{L��A0�#*�- �B�$bU�������-szC�� ��㰼��psڀ�"����L*������M���<1���}�"�<�U+0������}�l��_W���������$QhÎ)��75	�� p�\t��s�"�%Bo6q�����2��{��6�2=I�Qc/	T�V�E��D������W9C޺��������dќ��w�S�f�_�}8�#Փ'>kh��	`�v����_��5`��\�I"=����|�Z����Ycc&���^e-mC#H9,��X�C��4.�>��g� ��u~u{��s<�v�v�B� S� �m��&21
�=�#��_v��W.����u�ﰖ=��y�����<�s������(x,�3ݷX��|ƥ��J5���<�u�YU��v6~g����pE;7 ���%:(�R���~�8��A<-�/����4��o��op�H��:菃�p��Fu޳�=�.�������,�͏F> �H��H�W�`T�x,���-,m���,��[ ���+�%��M��ǘ�.��}��|q~-��;�ƽ*:�����T@���8��/.�I<^1�1Oϻ�V��H���_$�{�K��W3l�_�Kb9��C�Ͷ�j���Т�� u��s����6%��<�w����t�9�z�M�5K+8���v��z��=���*-�z6���fB5��~���;l���`[�d}�C( E ,D2`6�������q�&�'{\���<�i4� ��'�Ql�HF�[��6_G@.G� �a_?9kl�t
�5�CD�����d�6�$li��u�_s.o�su����c��D�������m ���th)a~xь������}����L�c3f��,u&tp+�L�zݢ�ᰳ䵓�u<d=6΅�[�U2gӾ�`��79���^OB+���i�z���&Җ��� H���e�Bλ9�<v��'5�Fz=�8,�3�qR��^�A/p^�*���!$4�b�	�<��WKw(��%o)�r=d%]�����=˹�a9Q�Q�c9[��rxi����Qq	� 	�8��7o�l沂�
����Ȕ��x�D¬<�t={$^_ڸ3�:y��*���Vj՞=^�G�<6I?0���X�ρ\L	��V�n���b7p 6�xT<|��l���ۼe����;�*��7d��F��:k�<�1!� �c��W��[&Bs�Z�n���Y��<��#�1�vo^�ѣ�z�h:{?rV|a|��]�Pԟ�h:Nu��@8�n�]̩@���.|��VI��yf����]�e��E��	LD�����˖e���*LV'����B�0a�ݹ}MQ�DuNǤ��M�ajG����|����=2l?r	&M{�������ZB	��@�ޡ6�tz2<؊�m��]N:�n���:���k��n-�zϵ���U�h�\�x�?�����-;6���[/�`Ёd8
��|�Ɛe�Cf��`??�������uJg��Z�w���&_`nT����V�4sx�c�q/D�8]*�	<� �  �|W�W�#�\�V_G~dhf��nm6%T���q0�Y�ޘl'����5�_�����5��*�c�����g.�UѬL�:x�`�q^o��)q^�9쓱3�G��1����y*���G���q��ϤJc�GɇC��X��aB�yd�%) �	��0	h���ͦ�z=[ů��9�m8���bY����,xPt���J��Ηj�:DPK崬w[\�iL�	��� �#^]���x0_�����>~����G���7���������x�#�䙅�K;?������8ݴ�ӕTMeuH�[,��{��U�w�S�tdV�\�Fh�a~���;�\�#Y����"h{i^*������<,����@��Al�k��ÍQ�qO�#,m��E;;<��(
������Œ~��a�6�-�'���z���#��K�=��"^+��K�޶�V^�о���Sȶ�z2ڮ�����^�"��Q;�<6���(E��q��� 'f�\FiK�a` ��D��5~�<��o��q���Ԥ>�_��y6v��a�cmNǓ����	!,B�T�L�#8g��eY�w�i�4���3���R	2����.���U�����{���<�~�!�7��n�����d��gFP�%����Sǭ���ǳ+�9��W�$R/i���(�n�ÿ	/N�B�3�o:�"Z[{���g��qܹ�ՓL�xϼ��m�����|=�phl4;���u��Vne���_��2рel;���}P�U���J;��MH��[EE:X#��3I��{��!v^$��䙘�:A<�� �:]�>żN}��g3W���x��������qd%�u�#"�y��b<�<_^�@'q���ޮl������m^�^(�7�I��ʔl��Ky�Y�_��_��_��_��_��_��_��_��_��_��_��_��_��_��_�����}�!��*��X�_��_��_��_��_��_��_��_��_��_��_��_��_��_��_���������g�������o2���&x��g��4�qM��J9�������� m$��F��������^�4�g-�-��Ŧ3�������*$�#�c���tn�{�!D��/tdǺx��AÂZ���7���h{s�ΐ�]�g@��Y@���І
,=���x�WF;�ͽ� |�����AG��U,ݼ�朕k��1�5���E��rb{#
M�y��~�di���Rd��_�n|仧�+Cc��tr���(�3��mh���^��=�#��?^�W~�ϛ��n�6���<?�����V�A��*�X�r��R#�g!P��:;'��>//��}9VV��lL0��-C����\t��pxN��ԻQ��F�G*�q_�jWB����w��F�`q�����+�&���J��T,�S&����>�w��B��"��N';�/x�Svp��p���6�'� !j����B�Y�����һ�ZZ|=A���p�6���ʦy%MG���FÔQoȀj�K���MN��Ou,��g;�k7K�^�x���.
�s}�ٖ�׿	��^����az�wh�r��5�]���%=8\���G�ZUI���͒������}>
xm��":�����uB�T�rG軮��c)V9��y��r,q��ǌm�ǎ�ҽX,ؿl>��3BA���{���<
�W���P[��.)|���x��P�EvF�7�kT�	~�D��=/�|���3M�;\/
���{i�CS��8�3�,��G�u/��f�����_v� �p��9����Fb�����l	-�")9��ٺ��{m��`L�/�/��./��S��)U3��#'���O?�W�X�o��/�+�f��*�Tns���F��l6�x�����}�ƞ�<p���+>/<��T�+���6�2"%l/Wf{وCh���󚣰�ψ*�"h�(K0�i- B��ȼ��Ccyo������f/��/?��YT���y�W�E�n�cU����x�����s}^�����s's���󪷜��?���p}޸��2��L���t�U3��{�aw���~���y����<���6��t���{dL�#Gr���2��'}����7�$�q��?_�7��?���y�N��M�>/�@����f�'��%�s�e�WC=�7� V[��v<���w;Zu���+qP\�̪ɖ��
����uz���S��Cs�%N1���t
5����1��hX�%��S5H�����1������vt��T���M��M"��4�G
��G��9��ٿ]���6���-T�]�g^��.�è�Y��¢p���rn���UǫU����~�q�p_B��
?_� Gn��c^�C� Z/�Saiޫ��O�wg>�?��w���� s�]������A�3iw���ogy�SWQq�qĦ��W�.��x���i�
Q��8n�K?mh��m�:r�~�F�*��*����� �`@�ɗW��6��덼���^!;0�/󹙗��0�﵎�Lz��n_-�=ޔ�5Fc��x�����d���|��&��#|�н��mpO�r���K�������M5o/���"π�������7�0~ׇ��բgv�#��>J�Th��4�+9����݂��v<^>6rrl�����װw��LG���K�����ncw����1%r`��x�.E�����da9��ގ�-v���=�[l����-�0��5�����`p`@�(F0��^`�%������<n	༖)���΂��,$R�� ̪������^-;y�m	h:����}g�A�����_g�- |xC=|���7��O��N�9�S0h���.�¾�ǆ�EtrJ�c�Nhj��$^��~�����w�y���d���� �^�@`ّpY��5�st�*���>Z��!M8%����{,]/nf���k��a*���������=�5�G��lAp���W�ʚ��iZ�b�5^K�0R\5��)�� H��ty1��lI��$0(1����T�h�f��G��.՜_ j�>�8ɮ[/R"b�i���G�d6��ۣW8XA�=W���36�����!�WS�5�b������+7@=x�T!-���u�Ӯ�n��.��w첀^Z���|v�Տo�>#h�!�^�UM�J$;�1 �B���#��/��`���'����rys���o�Kn��ˑ�l߀GT���^��xuxz�\�d�ׁ����M�K������$�?zqN���R���.�Vc����&`�wɡ�YjǪ6�;#Ү���:�!NK�(��w�iԺ�v]:1�&T��:��М�S�)�"��W���.���Nt��3zOx,���&�
����2B�wAF�އi*����U��UVj#"�	�}^��O���>���굓6�bH`��!�yz��r����x��d@@�^����%P�Eh�IQ�n�"jo`v�kk1��F�K�0�,@��q��P�i�D�{֯m�Oѫ:����=.����9�������]~m�qxس�+�׎7��Ƌ�hX��.�6��EA� ��4o�#�z�\���^���b��z@T�m;�!Ս��~�����v�f|Wl��_o���tE]��ŽV�2_���w����E��u������o��a`�ﺼ	�|~�s]HY����.�{��F���s6�w��\�go(^�]��K!�lQ�z ��U�.����~���?��m�v�v]�߮˃�>�����]�w���xcLXi���<�ь^�H��t]��������`�ZZ���������ۿ�� WX�     