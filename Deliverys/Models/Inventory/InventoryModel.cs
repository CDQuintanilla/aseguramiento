﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Deliverys.Models.Orders;

namespace Deliverys.Models.Inventory
{
    [Table("Inventory")]
    public class InventoryModel
    {
        public InventoryModel()
        {
            this.Orders = new HashSet<OdersModel>();
        }

        [Key]
        [Display(Name = "Código Producto")]
        public int idproduct { get; set; }

        [Display(Name = "Producto")]
        [Column(TypeName ="Varchar")]
        [StringLength(50)]
        [MinLength(4)]
        public string product { get; set; }

        [Display(Name = "Descripción")]
        [Column(TypeName = "Varchar")]
        [StringLength(150)]
        [MinLength(10)]
        public string description { get; set; }

        [Display(Name = "Stock")]
        public int stock { get; set; }

        public virtual ICollection<OdersModel> Orders { get; set; }
    }
}