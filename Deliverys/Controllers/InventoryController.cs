﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Deliverys.Context;
using Deliverys.Models.Inventory;

namespace Deliverys.Controllers
{
    public class InventoryController : Controller
    {
        private PostsqldbContext db = new PostsqldbContext();

        // GET: Inventory
        public ActionResult Index()
        {
            return View(db.inventario.ToList());
        }

        // GET: Inventory/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InventoryModel inventoryModel = db.inventario.Find(id);
            if (inventoryModel == null)
            {
                return HttpNotFound();
            }
            return View(inventoryModel);
        }

        // GET: Inventory/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Inventory/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idproduct,product,description,stock")] InventoryModel inventoryModel)
        {
            if (ModelState.IsValid)
            {
                db.inventario.Add(inventoryModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(inventoryModel);
        }

        // GET: Inventory/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InventoryModel inventoryModel = db.inventario.Find(id);
            if (inventoryModel == null)
            {
                return HttpNotFound();
            }
            return View(inventoryModel);
        }

        // POST: Inventory/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idproduct,product,description,stock")] InventoryModel inventoryModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(inventoryModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(inventoryModel);
        }

        // GET: Inventory/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            InventoryModel inventoryModel = db.inventario.Find(id);
            if (inventoryModel == null)
            {
                return HttpNotFound();
            }
            return View(inventoryModel);
        }

        // POST: Inventory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InventoryModel inventoryModel = db.inventario.Find(id);
            db.inventario.Remove(inventoryModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
