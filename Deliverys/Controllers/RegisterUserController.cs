﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Deliverys.Context;
using Deliverys.Models.Home;

namespace Deliverys.Controllers
{
    public class RegisterUserController : Controller
    {
        private PostsqldbContext db = new PostsqldbContext();

        // GET: RegisterUser
        public ActionResult Index()
        {
            return View(db.registrar.ToList());
        }

        // GET: RegisterUser/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterModel registerModel = db.registrar.Find(id);
            if (registerModel == null)
            {
                return HttpNotFound();
            }
            return View(registerModel);
        }

        // GET: RegisterUser/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RegisterUser/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "iduser,nombre,apellido,telefono,correo,contrasea,direccion")] RegisterModel registerModel)
        {
            if (ModelState.IsValid)
            {
                registerModel.tipopersona = false;
                db.registrar.Add(registerModel);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(registerModel);
        }

        // GET: RegisterUser/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterModel registerModel = db.registrar.Find(id);
            if (registerModel == null)
            {
                return HttpNotFound();
            }
            return View(registerModel);
        }

        // POST: RegisterUser/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que quiere enlazarse. Para obtener 
        // más detalles, vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "iduser,nombre,apellido,telefono,correo,contrasea,direccion")] RegisterModel registerModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registerModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(registerModel);
        }

        // GET: RegisterUser/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterModel registerModel = db.registrar.Find(id);
            if (registerModel == null)
            {
                return HttpNotFound();
            }
            return View(registerModel);
        }

        // POST: RegisterUser/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RegisterModel registerModel = db.registrar.Find(id);
            db.registrar.Remove(registerModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
