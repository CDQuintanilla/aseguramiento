﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using Deliverys.Context;
using Deliverys.Models.Home;

namespace Deliverys.Controllers
{
    public class RegisterClientController : Controller
    {
        private PostsqldbContext db = new PostsqldbContext();

        // GET: RegisterClient
        public ActionResult Index()
        {
            return View(db.registrar.ToList());
        }

        // GET: RegisterClient/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterModel registerModel = db.registrar.Find(id);
            if (registerModel == null)
            {
                return HttpNotFound();
            }
            return View(registerModel);
        }

        // GET: RegisterClient/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RegisterClient/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "iduser,nombre,apellido,telefono,correo,contrasea,direccion")] RegisterModel registerModel)
        {
            if (ModelState.IsValid)
            {
                registerModel.tipopersona = true;
                db.registrar.Add(registerModel);
                db.SaveChanges();
                return RedirectToAction("Create");
            }

            return View(registerModel);
        }

        // GET: RegisterClient/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterModel registerModel = db.registrar.Find(id);
            if (registerModel == null)
            {
                return HttpNotFound();
            }
            return View(registerModel);
        }

        // POST: RegisterClient/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "iduser,nombre,apellido,telefono,correo,contrasea,direccion")] RegisterModel registerModel)
        {
            if (ModelState.IsValid)
            {
                db.Entry(registerModel).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(registerModel);
        }

        // GET: RegisterClient/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RegisterModel registerModel = db.registrar.Find(id);
            if (registerModel == null)
            {
                return HttpNotFound();
            }
            return View(registerModel);
        }

        // POST: RegisterClient/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RegisterModel registerModel = db.registrar.Find(id);
            db.registrar.Remove(registerModel);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
