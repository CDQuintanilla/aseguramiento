﻿using Deliverys.Models.AddExit;
using Deliverys.Models.Delivery;
using Deliverys.Models.Home;
using Deliverys.Models.Inventory;
using Deliverys.Models.Orders;
using System.Data.Entity;

namespace Deliverys.Context
{
    public class PostsqldbContext : DbContext
    {
        public PostsqldbContext() : base ("ConectionBD")
        {
        }

        public DbSet<RegisterModel> registrar { get; set; }
        public DbSet<InventoryModel> inventario { get; set; }
        public DbSet<DeliveryModel> delivery { get; set; }
        public DbSet<OdersModel> OdersModels { get; set; }
        public DbSet<ExitProductModel> ExitProductModels { get; set; }
    }
}