﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Deliverys.Models.AddExit
{
    [Table("Exits")]
    public class ExitProductModel
    {
        [Key]
        public int idexit { get; set; }

        [Display(Name = "Fecha Entrega")]
        [Column(TypeName = "Date")]
        [Required]
        public DateTime salidaentrega { get; set; }

        [Display(Name = "Código de Repartidor")]
        public virtual int iddelivery { get; set; }

        [Display(Name = "Código de Pedido")]
        public virtual int idorder { get; set; }
    }
}